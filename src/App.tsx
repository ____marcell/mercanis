import Packages from './components/Packages'

function App() {
  return (
    <>
      <Packages />
    </>
  )
}

export default App;
