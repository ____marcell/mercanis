import { useState, useEffect } from 'react'
import debounce from 'debounce'
import { Flex, Box } from 'rebass'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components';
import { selectSearch, search, load, selectLoadingState } from '../../slices/packages'
import { Package } from '../../slices/packages/types'
import PackageDetails from '../PackageDetails'
import Loading from '../Loading'
import Input from '../Input'

declare module 'debounce';

const PackageList = styled.ul`
`

const PackageItem = styled.li`
`

const SearchForm = styled.div`
`



function App() {
  const [searchTerm, setSearchTerm] = useState<string>('')
  const [showDetails, setShowDetails] = useState<boolean>(false)
  const [currentPackage, setCurrentPackage] = useState<Package | undefined>(undefined)
  const packages = useSelector(selectSearch)
  const loadingState = useSelector(selectLoadingState)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(load())
  }, [dispatch])

  const debouncedSearch = debounce((value: any) => {
    setShowDetails(false)
    dispatch(search(value))
  }, 400)

  const handlePackageClick = (item: Package) => {
    setShowDetails(true)
    setCurrentPackage(item)
  } 

  return (
    <>
      <SearchForm>
        <Input 
          type="text" 
          name="searchName"
          onChange={(event) => { 
            setSearchTerm(event.target.value)
            debouncedSearch(event.target.value)
          }}
          value={searchTerm}/>
      </SearchForm>
      {loadingState === 'pending' &&
        <Loading></Loading>
      }
      {loadingState === 'idle' &&
        <Flex >
          <Box width="50%">
            <PackageList>
              {packages.map((item: Package) => (
                <PackageItem key={item.id} onClick={() => handlePackageClick(item)}>
                  <p>{item.name}</p>
                  <p>{item.headline}</p>
                </PackageItem>
              ))}
            </PackageList>
          </Box>
          <Box width="50%">
            {showDetails &&
              <PackageDetails currentPackage={currentPackage} />
            }
          </Box>
        </Flex>
      }
    </>
  )
}

export default App;
