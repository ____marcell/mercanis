import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components';
import { selectComments, create, setCurrentPackageId } from '../../slices/comments'
import { Comment } from '../../slices/comments/types'
import { useFormik } from 'formik'
import * as yup from 'yup';
import ErrorMessage from '../ErrorMessage'
import TextArea from '../TextArea'
import Text from '../Text'
import Button from '../Button'

const CommentList = styled.ul`
  
`

const CommentItem = styled.li`

`

const CommentForm = styled.form`
`

type Props = {
  currentPackageId: string
}

function Comments({ currentPackageId }: Props) {
  const comments = useSelector(selectComments)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setCurrentPackageId(currentPackageId)) 
  }, [dispatch, currentPackageId])

  const handleSubmitForm = (event: any) => {
    event.preventDefault()
    values.packageId = currentPackageId
    dispatch(create({ ...values }))
  }

  const {
    values,
    errors,
    touched,
    handleBlur,
    setFieldValue
  } = useFormik({
    validateOnMount: true,
    validationSchema: yup.object({
      body: yup.string().required('body is required')
    }),
    initialValues: {
      body: '',
      packageId: ''
    },
    onSubmit: handleSubmitForm 
  })

  return (
    <>
      <Text>Add Comment</Text>
      <CommentForm onSubmit={handleSubmitForm}>
        <TextArea
          name="body"
          onChange={(event: any) => setFieldValue('body', event.target.value)}
          onBlur={handleBlur}
          value={values.body}/>
        <ErrorMessage>{touched.body && errors.body}</ErrorMessage>
        <Button type="submit">Add</Button>
      </CommentForm>
      <CommentList>
        {comments.map(({ id, body }: Comment) => (
          <CommentItem key={id}>
            {body}
          </CommentItem>
        ))}
      </CommentList>
    </>
  )
}

export default Comments;
