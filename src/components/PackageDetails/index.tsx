import Comments from '../Comments'
import { Package } from '../../slices/packages/types'
import { Flex } from 'rebass';
import Text from '../Text'
import { List, ListItem } from '../List'

type Props = {
  currentPackage: Package | undefined;
}

const PackageDetails = ({ currentPackage }: Props) => {
  if (!currentPackage){
    return null 
  }
  const { 
    name, 
    description, 
    id, 
    license,
    created_at,
    version,
    dependencies,
    imports,
    authors
  } = currentPackage
  return (
    <>
      <Flex flexDirection="row">
        <Text>{name}</Text>
        <Text>{version}</Text>
      </Flex>
      <Flex flexDirection="column">
        <Text>{license}</Text>
        <Text>{created_at}</Text>
      </Flex>
      <Text>{description}</Text>
      <Flex flexDirection="row">
        <Text>Dependencies:</Text>
        <List>
          {dependencies.map((dependency) => (
            <ListItem key={dependency}>
              {dependency}
            </ListItem>
          ))}
        </List>
      </Flex>
      <Flex flexDirection="row">
        <Text>Imports:</Text>
        <List>
          {imports.map((item) => (
            <ListItem key={item}>
              {item}
            </ListItem>
          ))}
        </List>
      </Flex>
      <Flex flexDirection="row">
        <Text>Imports:</Text>
        <List>
          {authors.map((author) => (
            <ListItem key={author}>
              {author}
            </ListItem>
          ))}
        </List>
      </Flex>
      <hr/>
      <Comments currentPackageId={id} />
    </>
  )
} 

export default PackageDetails
