export type Package = {
  id: string;
  name: string;
  version: string;
  license: string;
  description: string;
  dependencies: string[];
  imports: string[];
  authors: string[];
  headline: string;
  created_at: string;
}
