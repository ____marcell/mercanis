import packages, {
  initialState,
  selectPackages,
  selectSearch,
  search
} from '../index';


import { initialState as commentsInitialState } from '../../comments'

import { RootState } from '../../../app/store';

const rootStateMock: RootState = {
  packages: initialState,
  comments: commentsInitialState 
}

describe('groups reducer', () => {
  it('it should handle initial state', () => {
    // @ts-expect-error:
    expect(packages(undefined, {})).toEqual(initialState);
  });

  it('it should select the packages', () => {
    expect(selectPackages(rootStateMock)).toEqual(initialState);
  })

  it('it should return the packages filter by the searched term', () => {
    let results = packages(initialState, {
      type: search.type,
      payload: 'odeqcdr' 
    })
    expect(results.searchName).toEqual('odeqcdr')
  })

  it('it should return the packages filter by the searched term', () => {
    rootStateMock.packages.searchName = 'odeqcdr'
    expect(selectSearch(rootStateMock)).toEqual([{
      id:"3",
      name: "DEQrmichie/odeqcdr",
    }])
  })
});
