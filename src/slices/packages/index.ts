import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store'
import { Package } from './types'

type State = {
  loading: 'idle' | 'pending';
  searchName: string;
  allPackages: Package[];
}

export const initialState: State = {
  loading: 'idle',
  searchName: '',
  allPackages: [
    {
      id:"1",
      name:"campbio/musicatk",
      headline:"Mutational Signature Comprehensive Analysis Toolkit",
      created_at:"2020-07-17T09:28:11.523Z",
      version:"2.9.1",
      license:"LGPL-3",
      dependencies:[
         "R (>= 4.0.0)",
         "AlessioNar/Rops"
      ],
      imports:[
         "dreamRs/shinyWidgets",
         "AlessioNar/Rops",
         "DEQrmichie/odeqcdr"
      ],
      authors:[
         "Scott Fortmann-Roe <scottfr@berkeley.edu>",
         "Bob Sponge",
         "Bruce Wayne <bruce.wayne@batman.com>"
      ],
      description:"Aenean at massa lectus. Donec condimentum nunc sit amet libero dapibus, in pulvinar elit vestibulum. In ac enim ac sem vulputate consequat nec sed ipsum. Vestibulum nulla purus, ultricies vitae orci eget, rhoncus luctus felis. Nam ac porttitor orci. Nunc elementum augue vitae justo vulputate tempor. Phasellus at erat et lectus pellentesque convallis ut a tellus. Nam nunc arcu, varius vel eros vitae, tincidunt pulvinar elit. Proin non velit nec libero convallis suscipit. Fusce vitae ipsum elementum, porttitor augue vel, ornare velit. Sed varius nisl ut vulputate pretium. Quisque sollicitudin, ex sed congue finibus, nibh massa venenatis ante, sodales sagittis nisi ligula a neque. Ut tincidunt felis ac erat vulputate pellentesque. Curabitur ante turpis, imperdiet sed velit id, ultrices tempus enim. Donec porta, mauris ut interdum varius, ante felis sodales mi, nec maximus mauris velit sed diam. Donec egestas, odio vitae ornare tristique, felis risus imperdiet diam, vitae porttitor sapien libero quis erat."
    },
    {
      id:"2",
      name:"AlessioNar/Rops",
      headline:"API access to EPO OPS service",
      created_at:"2020-06-16T09:28:11.523Z",
      version:"0.99.11",
      license:"GPL-3",
      dependencies:[
         "R (>= 3.1.0)",
         "campbio/musicatk"
      ],
      imports:[
         "dreamRs/shinyWidgets",
         "campbio/musicatk",
         "DEQrmichie/odeqcdr"
      ],
      authors:[
         "Scott Fortmann-Roe <scottfr@berkeley.edu>",
         "Patrick Smith",
         "Bruce Wayne <bruce.wayne@batman.com>"
      ],
      description:"Aenean at massa lectus. Donec condimentum nunc sit amet libero dapibus, in pulvinar elit vestibulum. In ac enim ac sem vulputate consequat nec sed ipsum. Vestibulum nulla purus, ultricies vitae orci eget, rhoncus luctus felis. Nam ac porttitor orci. Nunc elementum augue vitae justo vulputate tempor. Phasellus at erat et lectus pellentesque convallis ut a tellus. Nam nunc arcu, varius vel eros vitae, tincidunt pulvinar elit. Proin non velit nec libero convallis suscipit. Fusce vitae ipsum elementum, porttitor augue vel, ornare velit. Sed varius nisl ut vulputate pretium. Quisque sollicitudin, ex sed congue finibus, nibh massa venenatis ante, sodales sagittis nisi ligula a neque. Ut tincidunt felis ac erat vulputate pellentesque. Curabitur ante turpis, imperdiet sed velit id, ultrices tempus enim. Donec porta, mauris ut interdum varius, ante felis sodales mi, nec maximus mauris velit sed diam. Donec egestas, odio vitae ornare tristique, felis risus imperdiet diam, vitae porttitor sapien libero quis erat."
    }
  ]
}

export const packages = createSlice({
  name: 'packages',
  initialState,
  reducers: {
    loading: (state, _) => {
      if (state.loading === 'idle') {
        state.loading = 'pending'
      }
    }, 
    received(state, action) {
      if (state.loading === 'pending') {
        state.loading = 'idle'
        state.allPackages = action.payload
      }
    },
    search(state, action) {
      const { payload } = action
      state.searchName = payload
    }
  }
});

const { actions, reducer } = packages;

export const { loading, received, search } = actions;

export const selectSearch = ({ packages }: RootState) => {
  return packages.allPackages.filter((item: Package) => 
    new RegExp(packages.searchName, 'i').test(item.name) ||
    new RegExp(packages.searchName, 'i').test(item.headline))
}

export const selectLoadingState = ({ packages }: RootState) => packages.loading

const sortByDate = (a: Package, b: Package) => {
  let dateA = new Date(a.created_at)
  let dateB = new Date(b.created_at)
  if (dateA > dateB){
    return -1
  }
  if (dateA < dateB){
    return 1
  }
  return 0
}

export const load = () => async (dispatch: any) => {
  dispatch(loading(initialState))
  try {
    let packages: Package[] = (await (await fetch('https://600eda693bb1d100179e04dc.mockapi.io/api/v1/packages')).json());
    dispatch(received(packages.sort(sortByDate)))
  }catch(err) {
    console.error(err) 
  }
}

export default reducer;
