import comments, {
  initialState,
  selectComments,
  create,
  setCurrentPackageId
} from '../index';

import { initialState as packagesInitialState } from '../../packages'

import { Comment } from '../types'

import { RootState } from '../../../app/store';

const rootStateMock: RootState = {
  packages: packagesInitialState,
  comments: initialState
}

const mockComment: Comment = {
  id: `basdfasdf`,
  body: 'group name',
}

describe('groups reducer', () => {
  it('it should handle initial state', () => {
    // @ts-expect-error:
    expect(comments(undefined, {})).toEqual(initialState);
  });

  it('it should select the comments', () => {
    rootStateMock.comments.currentPackageId = '2'
    expect(selectComments(rootStateMock)).toEqual([{
      id: 'b8po2efBaZv8ai5Te6mzo',
      body: 'this is a comment',
      packageId: '2'
    }]);
  })

  it('it should handle CREATE', () => {
    const results = comments(undefined, {
      type: create.type,
      payload: mockComment
    });
    const { id } = results.allComments[results.allComments.length - 1];
    expect(results.allComments).toEqual([...initialState.allComments, { ...mockComment, id }]);
  });

  it('it should set the currentPacakgeId', () => {
    const results = comments(undefined, {
      type: setCurrentPackageId.type,
      payload: '3' 
    })

    expect(results.currentPackageId).toEqual('3')
  })

  it('it return the right comments', () => {
    rootStateMock.comments.currentPackageId = '3'
    expect(selectComments(rootStateMock)).toEqual([{
      id: '4w3NnfnJSTmfkuNuuwNa-',
      body: 'Another comment',
      packageId: '3'
    }])
  })
});
