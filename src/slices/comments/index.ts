import { createSlice, nanoid } from '@reduxjs/toolkit';
import { RootState } from '../../app/store'
import { Comment } from './types'

type State = {
  currentPackageId: string;
  allComments: Comment[];
}

export const initialState: State = {
  currentPackageId: '',
  allComments: [
    {
      id: 'b8po2efBaZv8ai5Te6mzo',
      body: 'this is a comment',
      packageId: '1'
    },
    {
      id: '4w3NnfnJSTmfkuNuuwNa-',
      body: 'Another comment',
      packageId: '2'
    },
  ]
}

export const comments = createSlice({
  name: 'comments',
  initialState,
  reducers: {
    create: (state, action) => {
      const { payload } = action;
      const id: string = nanoid();
      state.allComments.push({
        ...payload,
        id
      });
    },
    setCurrentPackageId: (state, action) => {
      const { payload } = action
      state.currentPackageId = payload
    }
  }
});

const { actions, reducer } = comments;

export const { create, setCurrentPackageId } = actions;

export const selectComments = ({ comments }: RootState): Comment[] => {
  return comments.allComments.filter((comment: Comment) => 
    comment.packageId === comments.currentPackageId) 
}

export default reducer;
