export type Comment = {
  id: string;
  body: string;
  packageId?: string;
}
