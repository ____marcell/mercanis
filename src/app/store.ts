import { persistStore, persistReducer } from 'redux-persist'
import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import packagesReducer from '../slices/packages';
import commentsReducer from '../slices/comments';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

export type RootState = {
  packages: ReturnType<typeof packagesReducer>;
  comments: ReturnType<typeof commentsReducer>;
}

const persistConfig = {
  key: 'root',
  storage
}

const persistedReducer = persistReducer(persistConfig, commentsReducer)

const combinedReducers = combineReducers({ packages: packagesReducer, comments: persistedReducer })

const configuration = () => {
  let store = configureStore({ 
    reducer: combinedReducers,
    middleware: getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['persist/PERSIST']
      }
    })
  })
  let persistor = persistStore(store)
  return { store, persistor }
}

export default configuration
